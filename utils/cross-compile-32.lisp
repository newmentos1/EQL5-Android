;;; For NDK versions below 19, you need to create a standalone toolchain.

(in-package :cl-user)

(pushnew :android *features*)

(require :cmp)

(defvar *ndk-toolchain* (ext:getenv "ANDROID_NDK_TOOLCHAIN"))
(defvar *ecl-android*   (ext:getenv "ECL_ANDROID_32"))

(setf c::*ecl-include-directory* (x:cc *ecl-android* "/include/")
      c::*ecl-library-directory* (x:cc *ecl-android* "/lib/"))

(defun ecl-config (flags)
  (read-line (ext:run-program (x:cc *ecl-android* "/bin/ecl-config")
                              (list flags))))

(setf c::*cc*              (let* ((%path (x:cc *ndk-toolchain* "/bin/armv7a-linux-androideabi~A-clang"))
                                  (path (or (probe-file (format nil %path 16)) ; >= ndk-19
                                            (probe-file (format nil %path "")) ; <= ndk-18
                                            (error "clang compiler not found"))))
                             (namestring path))
      c::*ld*              (x:cc *ndk-toolchain* "/bin/arm-linux-androideabi-ld")
      c::*ar*              (x:cc *ndk-toolchain* "/bin/arm-linux-androideabi-ar")
      c::*ranlib*          (x:cc *ndk-toolchain* "/bin/arm-linux-androideabi-ranlib")
      c::*cc-flags*        (x:cc (ecl-config "--cflags")
                                 " -DANDROID -DPLATFORM_ANDROID -O2 -fPIC -fno-common -D_THREAD_SAFE -I"
                                 *ecl-android* "/build/gmp")
      c::*ld-flags*        (x:cc "-L" *ecl-android* "/lib -lecl -ldl -lm "
                                 "-L" *ndk-toolchain* "/sysroot/usr/lib/arm-linux-androideabi/")
      c::*ld-rpath*        nil
      c::*ld-shared-flags* (x:cc "-shared " c::*ld-flags*)
      c::*ld-bundle-flags* c::*ld-shared-flags*)

(format t "~%*** cross compiling for 'arm' ***~%")

