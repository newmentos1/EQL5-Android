import QtQuick 2.10
import QtQuick.Controls 2.5
import EQL5 1.0

Rectangle {
    id: ipPage
    color: "black"

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 4

        // Swank
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            font.weight: Font.DemiBold
            font.pixelSize: 12
            color: "lightgray"
            text: "Swank / local IP"
        }
        RoundButton {
            anchors.horizontalCenter: parent.horizontalCenter
            objectName: "swank"
            font.bold: true
            text: "Start"

            onClicked: {
                text = "Waiting for IP..."
                ipPage.color = "orange"
                ipPage.opacity = 0.7
                Lisp.call("eql:qsleep", 0.1) // for above to be processed instantly
                Lisp.call("eql:start-swank")
                ipPage.color = "black"
                ipPage.opacity = 1
            }
        }

        // spacer
        Item {
            width: 1
            height: 5
        }

        // PC (remote) IP
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            font.weight: Font.DemiBold
            font.pixelSize: 12
            color: "lightgray"
            text: "PC / remote IP"
        }
        Row {
            Text {
                height: ip.height
                verticalAlignment: Text.AlignVCenter
                text: "192.168.1."
                color: "white"
            }
            Tumbler {
                id: ip
                objectName: "remote_ip"
                height: 68
                width: 30
                model: 256
                visibleItemCount: 5

                delegate: Text {
                    height: parent.height
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    opacity: 1.0 - Math.abs(Tumbler.displacement) / (ip.visibleItemCount / 1.7)
                    font.weight: Font.DemiBold
                    color: "white"
                    text: modelData
                }

                onCurrentIndexChanged: Lisp.call("heart-rate:save-ip")
            }
        }
    }
}
