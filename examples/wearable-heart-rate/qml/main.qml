import QtQuick 2.10
import QtQuick.Controls 2.5
import "ext/" as Ext

Rectangle {
    width: 210  // for desktop
    height: 210 // (see above)
    color: "black"

    SwipeView {
        id: view
        anchors.fill: parent
        orientation: Qt.Vertical
        currentIndex: 0

        Ext.HeartRate {}

        Ext.Swank {}
    }

    // page indicator

    PageIndicator {
        id: indicator
        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        height: 25
        count: view.count
        currentIndex: view.currentIndex

        delegate: Rectangle {
            implicitWidth: 8
            implicitHeight: 8
            radius: width / 2
            color: "white"
            opacity: index === indicator.currentIndex ? 0.9 : 0.4
        }
    }
}
