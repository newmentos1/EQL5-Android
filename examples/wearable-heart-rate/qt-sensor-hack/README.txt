
HOWTO
=====

Hack to add not yet officially supported sensors to Qt:

Just copy 'QtActivity.java' to path in 'path.txt' (for Qt 5.13),
alternatively apply patch 'git.diff'.

If you're interested in the details of this hack, see also:

../build/main.h                        # call Java from Qt
../lisp/ini.lisp                       # sensor function wrappers
../lisp/heart-rate.lisp                # permissions
../android-sources/AndroidManifest.xml # permissions



NOTE
====

The proper way to add new sensors to Qt/QML would be to

1) install Qt + sources
2) run the following script:
   ~/Qt5.13.2/5.13.2/Src/qtsensors/src/sensors$ ./make_sensor.pl QHeartRate

Then you would need to edit the generated files etc., but I didn't try it
myself.
