# Lisp
find lisp/ -name "*.o" -delete

# C++
rm -fr tmp
rm -f Makefile*

# apk
rm -fr android-build
