# be careful with option --install, as this would uninstall
# the app first, deleting all files (e.g. from Quicklisp)

~/Qt"$QT_VERSION"/"$QT_VERSION"/android_armv7/bin/androiddeployqt \
  --input android-libmy.so-deployment-settings.json \
  --output android-build \
  --gradle
