### Prepare

#### Android

Since a working (and patched) Swank version is already included, just eval
`(eql:start-swank)`, or simply `:s`, to start the server.

#### PC

On the PC you should preferably use Slime v2.24 (newer versions may work, but
will show a warning at startup).



### Connecting to android: 2 variants

Please note: if -- for any reason -- the connection can't be established the
first time you try, it seems best to restart the android REPL, before
attempting another try (I'm talking out of experience).

Also, stopping Swank on android and restarting it, without restarting the REPL
app too, seems not to work.

#### 1) Local: port forwarding

The `adb` command is normally located in `/opt/android/sdk/platform-tools/`.

You only need to physically connect your device via USB (no wlan needed).

* enable port forwarding

```
  $ adb forward tcp:4005 tcp:4005
```

* on the android REPL, run:

```
  :s ; (start-swank)
```

* connect from Emacs:

```
  M-X slime-connect RET RET (just use default values)
```



#### 2) Remote: use ssh tunnel

You'll need an ssh app, e.g. SSHDroid; run it and activate the ssh daemon.
It will tell you the IP address of your device.

* on the android REPL, run:

```
  (start-swank :interface "127.0.0.1")
```

* on the PC, create the ssh tunnel, using the IP (after `root@`) as shown in
  the ssh app

```
  $ ssh -L4005:127.0.0.1:4005 root@192.168.1.2 -p 2222
```

(the default password is `admin`)

* connect from Emacs:

```
  M-X slime-connect RET RET (just use default values)
```



### Test

To test if it works, you can try:

```
  (|pret
```

followed by `TAB`, which should complete to:

```
  (|prettyProductName.QSysInfo|)
```

This will print your OS version.

Just for fun you may try:

```
  (in-package :editor)

  (q> |text| *qml-command* "(+ 1 2)")

  (q! |append| *qml-command* (string #\Newline))
```



### Installing Quicklisp libraries

Since you can't really follow the progress while installing Quicklisp libraries
directly on the mobile device, it may be more convenient to install them
via the Slime REPL. In order to load/install Quicklisp from there, you need to
eval: `(eql:quicklisp)`



### Developing a GUI interactively, using QML

You can either use the EQL5 desktop version to build your app on the desktop
first, or you use the approach shown in example `../my`, which is fully
interactive for both Lisp and QML.

Example `../sokoban-repl` integrates a simple REPL, hidden by default (the
same REPL as in example `../my`).

See also file `lisp/qml-lisp` for interacting with QML, especially
`(qml:reload)`. Make sure to set an `objectName` to every QML item you
want to change interactively.

--

Please see also [README-2](README-2-BUILD-APK.md), **"Reload QML files from mobile device"**
