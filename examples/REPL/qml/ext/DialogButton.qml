import QtQuick 2.13
import QtQuick.Controls 2.13

Button {
  width: main.isPhone ? 42 : 48
  height: width
  font.family: fontAwesome.name
  font.pixelSize: 24
  flat: true
}
